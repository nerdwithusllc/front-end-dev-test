</main>


<div style='display: none'>
  <aside class="login-form grid-container" id="login-form">
    <div class="grid-x">
      <div class="cell">
        <h2>Member Login</h2>
        <form>
          <div class="grid-x input-group">
            <div class=" cell">
              <label for="email">
                <input type="email" name="" value="" placeholder="EMAIL"><br>
              </label>
            </div>
            <div class="cell">
              <label for="password">
                <input type="password" name="" value="" placeholder="PASSWORD"><br>
              </label>
            </div>
            <div class="cell">
              <p><a href="#forgot-pass">Forgot Your Password?</a></p>
            </div>
          </div>
          <div class="expanded button-group">
            <a href="#register" class="button left">Register</a>
            <a href="#signin" class="button right">Sign in</a>
          </div>
        </form>
      </div>
    </div>
  </div>

</div>

    <script src="bower_components/jquery/dist/jquery.js"></script>
    <script src="bower_components/what-input/dist/what-input.js"></script>
    <script src="bower_components/foundation-sites/dist/js/foundation.js"></script>
    <script src="./js/lib.js"></script>
    <script src="./js/app.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400|Roboto:400,700,900" rel="stylesheet">
    <script src="https://use.fontawesome.com/a584c89556.js"></script>
  </body>
</html>
