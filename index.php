<?php include('header.php'); ?>

<section class="grid-container padding-remove">
  <div class="grid-x hero align-middle">
    <div class="cell text-center">
      <h1>A Force For Good</h1>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
      laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
      beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit</p>
      <div class="button-group">
        <a href="#learn-more" class="button outline"> Learn More</a>
      </div>
    </div>
  </div>
</section>

<section class="grid-container three-up-content-row">
  <div class="grid-x">
    <div class="cell text-center">
      <h1>
        Member Advantages
      </h1>
    </div>

    <div class="grid-x small-up-1 medium-up-3 col-container medium-centered">
      <div class="cell">
        <figure>
          <img src="img/001-shirt.svg" alt="Merchandise" />
        </figure>
        <h2>
          Merchandise
        </h2>
        <p>
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
          laudantium
        </p>
      </div>
      <div class="cell">
        <figure>
          <img src="img/002-support.svg" alt="Support" />
        </figure>
        <h2>
          Customer Support
        </h2>
        <p>
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
          laudantium
        </p>
      </div>
      <div class="cell">
        <figure>
          <img src="img/004-shield.svg" alt="Security" />
        </figure>
        <h2>
          Security
        </h2>
        <p>
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
          laudantium
        </p>
      </div>
    </div>
  </div>
  <div class="grid-x">
    <div class="cell">
      <div class="button-group align-center">
        <a href="#joinnow" class="button red" data-featherlight="#login-form">Join Now</a>
      </div>
    </div>
  </div>
</section>


<?php include('footer.php'); ?>
