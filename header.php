<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Front End Test - Voltagead</title>
    <link rel="stylesheet" href="css/app.css">
  </head>
  <body>


<header class="fixed-nav">
  <div class="site-header grid-container padding-remove">
    <div class="grid-x align-middle small-margin-collapse">
      <div class="cell small-12 medium-8 large-6">
        <a href="/" class="logo-emblem">
          <img src="img/voltage.svg" alt="lighting bold" />
        </a>
        <a href="/" class="logo-text">
          CLUB VOLTAGE
        </a>
      </div>
      <nav class="cell medium-4 large-6 show-for-medium">
        <a href="#store" class="nav-link">
          Store
        </a>
        <a href="#about" class="nav-link">
          About Us
        </a>
        <a href="#login-form" class="button red nav-button" data-featherlight="#login-form">
          Login / Sign Up
        </a>
      </nav>
    </div>
    <a href="#menu" class="menu-toggle show-for-small-only">
      <i class="fa fa-bars" aria-hidden="true"></i>
    </a>
  </div>
</header>

<main role="main">
  <a class="mobile-login show-for-small-only" data-featherlight="#login-form">
    Login / Sign Up
  </a>
