####This project test was completed using the following libraries and frameworks
* Foundation For Sites 6 http://zurb.founation.com/
 * Foundation CLI tool
* Featherlight JS https://noelboss.github.io/featherlight/
* SASS LANG http://sass-lang.com/
* GIT https://git-scm.com/
* NPM & Bower respectively
 * `$: sudo npm install` in root
 * `$: bower install` in root


Live Link: [http://client.nerdwith.us/voltage/](http://client.nerdwith.us/voltage)
